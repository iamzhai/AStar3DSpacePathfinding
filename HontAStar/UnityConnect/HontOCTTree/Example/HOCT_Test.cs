﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Hont;

public class TestOCTItems : IOCTItem<TestOCTItems>
{
    public Vector3 Position { get; set; }

    public TestOCTItems Value { get; set; }
}

public class HOCT_Test : MonoBehaviour
{
    public Bounds size;
    public int treeDepth = 2;
    HontOCTTree<TestOCTItems> mOCTTree;

    public HontOCTTree<TestOCTItems> OCTTree { get { return mOCTTree; } }


    void Awake()
    {
        mOCTTree = new HontOCTTree<TestOCTItems>(size, treeDepth);
    }

    [ContextMenu("Random Emitte")]
    void RandomEmitte()
    {
        for (int i = 0; i < 1; i++)
        {
            var x = UnityEngine.Random.Range(0, size.size.x);
            var y = UnityEngine.Random.Range(0, size.size.y);
            var z = UnityEngine.Random.Range(0, size.size.z);

            var item = new TestOCTItems() { Position = size.min + new Vector3(x, y, z) };

            mOCTTree.Root.AddItem(item);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(size.center, size.size);

        var oldColor = Gizmos.color;

        //Gizmos.color = new Color(0, 0, 1, 0.2f);

        if (!Application.isPlaying) return;

        Action<ReadOnlyCollection<OCTNode<TestOCTItems>>> r = null;

        r = (collection) =>
        {
            foreach (var node in collection)
            {
                if (node.Items.Count > 0)
                {
                    foreach (var item in node.Items)
                    {
                        Gizmos.DrawSphere(item.Position, 3f);
                    }
                }

                if (node.TotalItemsCount > 0)
                {
                    Gizmos.DrawWireCube(node.Bounds.center, node.Bounds.size);
                }

                if (node.HasChildren)
                    r(node.Childrens);
            }
        };

        r(mOCTTree.Root.Childrens);

        Gizmos.color = oldColor;
    }
}
